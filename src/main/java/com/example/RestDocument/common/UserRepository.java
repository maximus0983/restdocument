package com.example.RestDocument.common;


import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Resource, Long> {
    Resource getFirstById(Long id);
}
