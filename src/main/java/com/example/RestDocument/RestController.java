package com.example.RestDocument;

import com.example.RestDocument.common.Resource;
import com.example.RestDocument.common.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.List;

@Component
@org.springframework.web.bind.annotation.RestController
@MultipartConfig
@RequestMapping(value = "/document")
public class RestController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/{id}")
    public Resource accountById(@PathVariable("id") long id) {
        return userRepository.getFirstById(id);
    }

    @GetMapping("/alldocs")
    public List<Resource> allDocs() {
        return userRepository.findAll();
    }

    @PostMapping("/fileupload")
    public ResponseEntity addDoc(@RequestParam("file") MultipartFile file, @RequestParam("digitalSign") MultipartFile digitalSign) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(this.getClass().getResourceAsStream("/publicKey.p12"), "changeit".toCharArray());
        Certificate certificate = keyStore.getCertificate("publicKey");
        PublicKey publicKey = certificate.getPublicKey();
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(publicKey);
        byte[] messageBytes = file.getBytes();
        signature.update(messageBytes);
        if (signature.verify(digitalSign.getBytes())) {
            Resource resource = readFile(file);
            userRepository.save(resource);
        }
        return new ResponseEntity("Successfully uploaded - " + file.getName(), new HttpHeaders(), HttpStatus.OK);
    }

    private Resource readFile(MultipartFile file) {
        String content = null;
        try {
            content = new String(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Resource(content);
    }
}